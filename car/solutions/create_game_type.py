
from bson.objectid import ObjectId
import sys
sys.path.append('..')
from databases.client import MongoLsv

mongo_lsv = MongoLsv()
print(mongo_lsv.list_dbs())
print(mongo_lsv.list_collections(db_name="video_games"))

print(
    mongo_lsv.create_new_record_in_collection(
        db_name="video_games",
        collection="games_types",
        record={"label": "Actions", "value": "500000"},
    )
)

print(
    mongo_lsv.create_new_record_in_collection(
        db_name="video_games",
        collection="games_types",
        record={"label": "Deports", "value": "400000"},
    )
)

print(
    mongo_lsv.create_new_record_in_collection(
        db_name="video_games",
        collection="games_types",
        record={"label": "Avergers", "value": "600000"},
    )
)

print(
    mongo_lsv.create_new_record_in_collection(
        db_name="video_games",
        collection="games_types",
        record={"label": "Fits", "value": "800000"},
    )
)

print(
    mongo_lsv.get_records_from_collection(
        db_name="video_games", collection="games_types"
    )
)