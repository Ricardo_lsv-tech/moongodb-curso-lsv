from bson.objectid import ObjectId
import sys
sys.path.append('..')
from databases.client import MongoLsv

mongo_lsv = MongoLsv()
print(mongo_lsv.list_dbs())
print(mongo_lsv.list_collections(db_name="video_games"))


print(
    mongo_lsv.get_records_from_collection(
        db_name="video_games", collection="games"
    )
)

print(
    mongo_lsv.delete_record_in_collection(
        db_name="video_games",
        collection="games",
        record_id="62b0f47eea38da43e9f00d20",
    )
)

print(
    mongo_lsv.get_records_from_collection(
        db_name="video_games", collection="games"
    )
)