from bson.objectid import ObjectId
import sys
sys.path.append('..')
from databases.client import MongoLsv

mongo_lsv = MongoLsv()
print(mongo_lsv.list_dbs())
print(mongo_lsv.list_collections(db_name="video_games"))

print(
    mongo_lsv.create_new_record_in_collection(
        db_name="video_games",
        collection="games",
        record={"name": "FiFa", "description": "games of futball", "type": "deports"},
    )
)

print(
    mongo_lsv.create_new_record_in_collection(
        db_name="video_games",
        collection="games",
        record={"name": "Jumanji", "description": "games Avergers", "type": "avergers"},
    )
)

print(
    mongo_lsv.create_new_record_in_collection(
        db_name="video_games",
        collection="games",
        record={"name": "Call of Duties", "description": "Actions ganes", "type": "actions"},
    )
)

print(
    mongo_lsv.create_new_record_in_collection(
        db_name="video_games",
        collection="games",
        record={"name": "fast and furius", "description": "Actions games", "type": "actions"},
    )
)

print(
    mongo_lsv.create_new_record_in_collection(
        db_name="video_games",
        collection="games",
        record={"name": "harry potter", "description": "Avergers games", "type": "avergers"},
    )
)

print(
    mongo_lsv.create_new_record_in_collection(
        db_name="video_games",
        collection="games",
        record={"name": "basketball", "description": "Deports games", "type": "deports"},
    )
)



print(
    mongo_lsv.get_records_from_collection(
        db_name="video_games", collection="games"
    )
)
