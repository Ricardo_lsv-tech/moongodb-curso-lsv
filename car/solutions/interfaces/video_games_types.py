from dataclasses import asdict, dataclass
from datetime import datetime

class VideoGamesTypes:
    """
    VideoGamesTypes
    """
    uuid: str
    label: str
    value: str

    def to_dict(self) -> dict:
        return asdict(self)