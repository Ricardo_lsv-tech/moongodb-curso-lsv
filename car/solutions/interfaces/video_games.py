
from dataclasses import asdict, dataclass
from datetime import datetime

from car.solutions.interfaces.video_games_types import VideoGamesTypes

@dataclass
class VideoGames:
    """
    VideoGames
    """
    uuid: str
    name: str
    description: str
    type: VideoGamesTypes  # puede llevar uuid o id de video_games_types
    created_at: datetime
    modified_at: datetime

    def to_dict(self) -> dict:
        return asdict(self)

    @property
    def get_type(self):
        return self.type.label
