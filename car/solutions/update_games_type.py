from bson.objectid import ObjectId
import sys
sys.path.append('..')
from databases.client import MongoLsv

mongo_lsv = MongoLsv()
print(mongo_lsv.list_dbs())
print(mongo_lsv.list_collections(db_name="video_games"))


print(
    mongo_lsv.get_records_from_collection(
        db_name="video_games", collection="games_types"
    )
)


print(
    mongo_lsv.update_record_in_collection(
        db_name="video_games",
        collection="games_types",
        record_query={"_id": ObjectId("62b0f2d291a3fc2f01dd3a4d")},
        record_new_value={"label": "Actios Hard"},
    )
)

print(
    mongo_lsv.get_records_from_collection(
        db_name="video_games", collection="games_types"
    )
)